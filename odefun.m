%�utomatic Control Systems 3
%Project 1
%Author: Ioannis Papas 9218

function dxdt = odefun(t,x,input)
%input is to decide the input for the system. If 1 input is r=1, else if 0
syms t
if(input==1)
    r=1;
elseif (input==0)
    r=1.2*t;
end
dr=matlabFunction(diff(r,t));
dr2=matlabFunction(diff(dr,t));
dxdt = zeros(2,1);
dxdt(1)=x(2);
dxdt(2)=-x(2)-4*x(1)+dr()+dr2(); 
end