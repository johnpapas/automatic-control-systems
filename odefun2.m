%�utomatic Control Systems 3
%Project 2
%Author: Ioannis Papas 9218

function dxdt = odefun2(t,x)
%Function that simulates the 4 situation variables

dxdt(1)=x(3);
dxdt(2)=x(4);
if t<=5
    dxdt(3)=(1.9845*pi/18)*cos(0.63*t)+20*((3.15*pi/18)*sin(0.63*t)-x(3))+100*((-pi/2)+(5*pi/18)*(1-cos(0.63*t))-x(1));
    dxdt(4)=(-0.3969*pi/3)*cos(0.63*t)+20*((-0.63*pi/3)*sin(0.63*t)-x(4))+100*((17*pi/18)-(pi/3)*(1-cos(0.63*t))-x(2));
else
    dxdt(3)=-20*x(3)+100*((pi/18)-x(1));
    dxdt(4)=-20*x(4)+100*((5*pi/18)-x(2));
end


dxdt=dxdt';


end
