%�utomatic Control Systems 3
%Project 1
%Author: Ioannis Papas 9218

function dxdt = odefunNx(t,x,V,input)
%input is to decide the input for the system. If 1 input is r=1, else if 0

syms t
if(input==1)
    r=1;
elseif (input==0)
    r=V*t;
end
dr=matlabFunction(diff(r,t));
dr2=matlabFunction(diff(dr,t));
dxdt = zeros(2,1);
e0=0.2;
if (-e0<=x(1) && x(1)<=e0)
    a=0.06;
else
    a=1;
end

dxdt(1)=x(2);
dxdt(2)=-x(2)-4*a*x(1)+0.4+dr();

end