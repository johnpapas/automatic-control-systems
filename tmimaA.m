%�utomatic Control Systems 3
%Project 1
%Author: Ioannis Papas 9218

%%
%1st question (without N(x))
%(-2,1.5) , (-2.5,0.8) , (1.5,2) , (0.2,1.8) , (2.5,-0.8) , (2,-2) ,(-0.2,-1.8) ��� (-1,-2.5).
x0=[-1,-2.5];  %Change the initial conditions from here
tspan=0:0.01:20;

[t,x]=ode45(@(t,y) odefun(t,y,0), tspan, x0); %Odefun 1 for r=1, 0 for r=1.2t
figure(1)
clf
subplot(1,2,1)
plot(x(:,1),x(:,2))
hold on
plot(x0(1),x0(2),'-o',x(length(x(:,1)),1),x(length(x(:,1)),2), '-o')
title(sprintf("Phase Portrait for initial conditions: [%.2f %.2f]", x0(1), x0(2)))
xlabel("Error")
ylabel("Derivative of error")
subplot(1,2,2)
plot(t,x(:,1),t,x(:,2))
title(sprintf("Time response of variables for initial conditions: [%.2f %.2f]", x0(1), x0(2)))
xlabel("Time(s)")
legend("x1", "x2")

%%
%2nd question (with N(x))
xInit=[[-2 1.5] ;[-2.5,0.8] ;[1.5,2] ;[0.2,1.8] ;[2.5,-0.8] ;[2,-2] ;[-0.2,-1.8] ;[-1,-2.5]];
tspan=0:0.01:20;
for i=1:8
    x0=xInit(i,:); %Change the initial conditions from here
    V=0.4;

    [t,x1]=ode45(@(t,y) odefunNx(t,y,V,0), tspan, x0); %Odefun 1 for r=1, 0 for r=Vt
    figure(i)
    clf
    subplot(1,2,1)
    plot(x1(:,1),x1(:,2))
    hold on
    plot(x0(1),x0(2),'-o',x1(length(x1(:,1)),1),x1(length(x1(:,1)),2), '-o')
    title(sprintf("Phase Portrait for initial conditions: [%.2f %.2f]", x0(1), x0(2)))
    xlabel("Error")
    ylabel("Derivative of error")
    subplot(1,2,2)
    plot(t,x1(:,1),t,x1(:,2))
    title(sprintf("Time response of variables for initial conditions: [%.2f %.2f]", x0(1), x0(2)))
    xlabel("Time(s)")
    legend("x1", "x2")
end

