%�utomatic Control Systems 3
%Project 2
%Author: Ioannis Papas 9218

%%
%First question controller and tracking error
%Constants
m1=6;
m2=4;
L1=0.5;
L2=0.4;
Lc1=0.2;
Lc2=0.4;
Iz1=0.43;
Iz2=0.05;
g=9.81;

x0=[-87*pi/180,167*pi/180,0,0];  
tspan=0:0.01:10;
nS=length(tspan);

%The solution of the differential equation
[tV,x]=ode45(@(t,y) odefun2(t,y), tspan, x0); 
x1=x(:,1);
x2=x(:,2);
x3=x(:,3);
x4=x(:,4);

%Tracking output of 1st angle
qd1=(-pi/2)+(5*pi/18)*(1-cos(0.63*tspan(tspan<=5.01)));
k1=length(qd1);
qd1(k1:nS)=pi/18;

%Tracking output of 2nd angle
qd2=(17*pi/18)-(pi/3)*(1-cos(0.63*tspan(tspan<=5.01)));
k2=length(qd2);
qd2(k2:nS)=5*pi/18;

%Tracking output of 1st angular velocity (derrivative of qd1)
derqd1=(3.15*pi/18)*sin(0.63*tspan(tspan<=5.01));
k3=length(derqd1);
derqd1(k3:nS)=0;

%Tracking output of 2nd angular velocity (derrivative of qd2)
derqd2=(-0.63*pi/3)*sin(0.63*tspan(tspan<=5.01));
k4=length(derqd2);
derqd2(k3:nS)=0;

%The controller
u=zeros(nS,2);
for t=1:nS
    C=[[(-m2*L1*Lc2*sin(x2(t))*x4(t)) (-m2*L1*Lc2*sin(x2(t))*(x4(t)+x3(t)))]; [(m2*L1*Lc2*sin(x2(t)).*x3(t)) 0]];
    G=[(m2*Lc2*g*cos(x2(t)+x1(t))+(m2*L1+m1*Lc1)*g*cos(x1(t))); m2*Lc2*g*cos(x2(t)+x1(t))];
    H=[[(m2*(Lc2^2+2*L1*Lc2*cos(x2(t))+L1^2)+Lc2^2*m1+Iz2+Iz1) (m2*Lc2^2+2*L1*Lc2*cos(x2(t))+Iz2)];[(m2*Lc2^2+2*L1*Lc2*cos(x2(t))+Iz2) (m2*Lc2^2+Iz2)]];
    if t<=5
        tmp1=(1.9845*pi/18)*cos(0.63*t)+20*((3.15*pi/18)*sin(0.63*t)-x3(t))+100*((-pi/2)+(5*pi/18)*(1-cos(0.63*t))-x1(t));
        tmp2=(-0.3969*pi/18)*cos(0.63*t)+20*((-0.63*pi/18)*sin(0.63*t)-x4(t))+100*((17*pi/18)-(pi/3)*(1-cos(0.63*t))-x2(t));
    else
        tmp1=-20*x(3)+100*((pi/18)-x1(t));
        tmp2=-20*x(4)+100*((5*pi/18)-x2(t));
    end
    tmp=[tmp1;tmp2];
    u(t,:)=C*[x3(t);x4(t)]+G+H*tmp;
end
    
 %Plotting
 figure(1)
 plot(tV,180*x1/pi,tV,180*x2/pi)
 hold on
 plot(tV,180*qd1/pi,tV,180*qd2/pi)
 title("Angles and the tracking outputs")
 xlabel("Time(s)")
 ylabel("Degrees")
 legend("q1", "q2", "qd1", "qd2")
 
 figure(2)
 plot(tV,180*x3/pi,tV,180*x4/pi)
 hold on
 plot(tV,180*derqd1/pi,tV,180*derqd2/pi)
 title("Angular velocity and the tracking outputs")
 xlabel("Time(s)")
 ylabel("Degrees/sec")
 legend("q1'", "q2'", "qd1'", "qd2'")
 
 figure(3)
 plot(tV,180*x1/pi-180*qd1'/pi,tV,180*x2/pi-180*qd2'/pi)
 title("Position Tracking error")
 xlabel("Time(s)")
 ylabel("Degrees")
 legend("e1", "e2")
 
 figure(4)
 plot(tV,180*x3/pi-180*derqd1'/pi,tV,180*x4/pi-180*derqd2'/pi)
 title("Velocity Tracking error")
 xlabel("Time(s)")
 ylabel("Degrees/sec")
 legend("e1'", "e2'")
 
 figure(5)
 plot(tV,u(:,1),tV,u(:,2))
 title("Plot of the controller")
 xlabel("Time(s)")
 legend("u1", "u2")
 
 
 
 